Vue.component("heading",
    {   
        props:["deleted"],
        render: function(createElement){
            return createElement('div',
            {
                class:{
                    h1:true
                }
            },
            this.deleted ? "Deleted items" : "Todo List");
        }
        
});

Vue.component("add-item-bar",{
    template:`
    <div class="input-group mt-5 mb-5">
        <input type="text" placeholder="Add Item..." class="form-control rounded-0" id="add-item-text" v-model="inputText">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary rounded-0" type="button" id="btn-add-item" v-on:click="onAddItemClick">Add</button>
        </div>
    </div>
    `,
    data:function(){
        return{
            inputText:""
        }
    },
    methods:{
        onAddItemClick:function(){
            if(this.inputText != null && this.inputText != ""){
                this.$emit("additem",this.inputText);
                this.inputText = "";
            }
        }
    }
});

Vue.component("todo-item",{
    props:["todo","deleted"],
    template:`
    <div class="todo-item pt-3 pb-3 px-3">
        <div class="custom-control custom-checkbox d-flex">
            <input type="checkbox" class="custom-control-input" v-bind:id="todo.id" v-model="todo.checked" v-on:change="onCheckboxChanged">
            <label class="custom-control-label" v-bind:class="{ checked : todo.checked }" v-bind:for="todo.id">{{todo.text}}</label>
            <span class="material-icons ml-auto" id="btn-delete-item" v-if="!deleted" v-on:click="onDeleteBtnClick">delete</span>
        </div>
    </div>
    `,
    methods:{
        onCheckboxChanged:function(){
            this.$emit("updateitem",this.todo);
        },
        onDeleteBtnClick: function(){
            this.$emit("deleteitem",this.todo);
        }
    }
});

Vue.component("btn-show-deleted",{
    props:["showdeleted"],
    render:function(createElement){
        return createElement(
            'div',
            {
                class:{
                    btn: true,
                    "btn-outline-secondary": true,
                    "rounded-0":true 
                },

                domProps:{
                    innerHTML: this.showdeleted ? "Show current items" : "Show deleted items"
                }
            }
        );
    }
})

let app = new Vue({
    el: "#container",
    data:{
        items:[],
        deletedItems:[],
        showingDeletedItems: false
    },
    methods:{
        addItem:function(inputText){
            this.items.push({
                id: parseInt(Math.random()*1000),
                text:inputText,
                checked:false
            });
            this.updateLocalStorage();
        },
        updateItem: function(item){
            this.items.forEach(element => {
                if(element.id == item.id){
                    element.checked = item.checked;
                }
            });
            this.updateLocalStorage();
        },
        deleteItem: function(item){
            let i = 0;
            this.items.forEach((val,index) => {
                if(item.id == val.id){
                    i = index;
                }
            });
            let deleted = this.items[i];
            this.deletedItems.push(deleted);
            this.items.splice(i,1)
            this.updateLocalStorage();
        },
        updateLocalStorage: function(){
            if(typeof(Storage) !== "undefined"){
                localStorage.setItem("todolist",JSON.stringify(this.items));
                localStorage.setItem("todolist-deleted",JSON.stringify(this.deletedItems));
            }
        },
        switchItemView: function(){
            this.showingDeletedItems = !this.showingDeletedItems;   

            let temp = this.items;
            this.items = this.deletedItems;
            this.deletedItems = temp;
        }
    },

    mounted:function(){
        if(typeof(Storage) !== "undefined"){
            if(localStorage.getItem("todolist") != null){
                this.items = JSON.parse(localStorage.getItem("todolist"));
                if(!this.items) this.items = [];

                this.deletedItems = JSON.parse(localStorage.getItem("todolist-deleted"));
                if(!this.deletedItems) this.deletedItems = [];
            }
        }
    }

})